'use strict';
~ function() {
    var
	ad = document.getElementById('mainContent');
  
    window.init = function(){
        var tl = new TimelineMax();

        tl
		.set(ad,{perspective:1000,force3D:true})
        tl.addLabel('frameOne','+=2')

        .to('#copyOne', 0.5, {x:-275 ,rotation:0.1, ease:Power1.easeInOut},'frameOne')
        .to('#cameraOne', 0.5, {opacity:0, ease:Power1.easeInOut}, 'frameOne+=0.3')

        tl.addLabel('frameTwo','-=0.5')
        .to('#cameraTwo', 0.3, {opacity:1, ease:Power1.easeInOut}, 'frameTwo')
        .to('#copyTwo', 0.5, {x:0 ,rotation:0.01, ease:Power1.easeInOut},'frameTwo')

        tl.addLabel('frameThree','+=2')    
        .to('#blurCameraOne', 0.5, {opacity:1, rotation:0.01, ease:Power1.easeInOut},'frameThree')
        .to('#cameraTwo', 0.2, {opacity:0, ease:Power1.easeInOut},'frameThree+=0.3')
        .to('#blurCameraTwo', 0.5, {opacity:1, rotation:0.01, ease:Power1.easeInOut},'frameThree-=0.3')
        .to('#blurCameraOne', 0.5, {opacity:0, ease:Power1.easeInOut},'frameThree-=5')
        .to('#cameraThree', 0.5, {opacity:1, ease:Power1.easeInOut},'frameThree+=0.1')

        tl.addLabel('frameFour','+=0')
        .to('#blurCameraTwo', 0.2, {opacity:0, ease:Power1.easeInOut},'frameFour')
        .to('#copyTwo', 0.5,{x:-300,rotation:0.01, ease:Power1.easeInOut},'frameFour-=.7 ')
        .to('#copyThree', 0.5, {x:0 ,rotation:0.01, ease:Power1.easeInOut},'frameFour-=.5')
        .set(['#blurCameraOne','#blurCameraTwo'],{opacity:0},'frameFour')
    
        .to('#cameraThree', 0.5, {rotation:180, ease:Power1.easeInOut},'frameFour+=1.9')
        .to('#copyThree', 0.5, {x:-300 ,rotation:0.01, ease:Power1.easeInOut},'frameFour+=1.7')
        .to('#copyFour', 0.5, {x:0 ,rotation:0.01, ease:Power1.easeInOut},'frameFour+=1.8')
        .to('#copyFour', 0.5, {x:-300 ,rotation:0.01, ease:Power1.easeInOut},'frameFour+=3.5')

        tl.addLabel('frameFive','+=0')
        .to('#cameraThree', 1, {scale:0.316,  y:2, x:119, ease:Power1.easeInOut})
        .to('#cameraThree', 0.5, {opacity:0, ease:Power1.easeInOut},'frameFive+=0.9')
        .to('#copyFive', 0.5, {opacity:1, ease:Power1.easeInOut},'frameFive+=0.9')
        .to('#smallCopy', 0.5, {opacity:1, ease:Power1.easeInOut},'frameFive+=2.2')
        .to('#cta', 0.5, {opacity:1, ease:Power1.easeInOut}) 
    }
    
}();


